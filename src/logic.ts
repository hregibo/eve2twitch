import { MessageQueue } from "@hregibo/message-queue";
import Logger from "./logger";
import { IParsedMessage } from "twsc";
import * as Queries from "./queries";
import { SuperAgentRequest, get, post } from "superagent";
import { query } from "@hregibo/pg-dbc";
import { format } from "util";
export interface ESICharacter {
  id?: number;
  alliance_id?: number;
  birthday: string;
  bloodline_id: number;
  corporation_id?: number;
  description?: string;
  faction_id?: number;
  gender: "male" | "female";
  name: string;
  race_id: number;
  security_status?: number;
  title?: string;
}
export const baseURI = "https://esi.evetech.net/latest";
export const applyBaseHeaders = (
  request: SuperAgentRequest
): SuperAgentRequest => {
  return request
    .set("accept", "application/json")
    .set("accept-language", "en")
    .set("cache-control", "no-cache")
    .set("user-agent", format("EVE2TWITCH (Contact Character: Aenoa Exhulis)"));
};

export interface ESIAuthToken {
  access_token: string;
  refresh_token: string;
  expires_in: number;
  character_id: "Bearer";
  created_at?: string;
}
const _channelCache = new Map<string, string>();

export interface ITwitchUser {
  id: string;
  login?: string;
  display?: string;
  ign: string;
  eve_id: number | null;
  created_at?: string;
  updated_at?: string;
}
export interface IGetUserParameters {
  display?: string;
  id?: string;
}
export const createBaseQueue = (queue: MessageQueue) => {
  queue.setQueue({
    delay: 3000,
    name: "base",
    priority: 1,
  });
};
export const createQueueForChannel = (queue: MessageQueue, channel: any) => {
  queue.setQueue({
    delay: 1550,
    name: channel.name,
    priority: 10,
  });
};
export const triggerJoinRequests = async (queue: MessageQueue) => {
  const channels = [];

  const result = await query(Queries.getChannels, []);
  channels.push(...result.rows);

  for (const channel of channels) {
    _channelCache.set(`#${channel.name}`, channel.id);
    Logger.debug(channel, "enqueueing the join request for this channel");
    queue.emit("enqueue", {
      message: `JOIN #${channel.name}`,
      priority: 10,
      queue: "base",
    });
    createQueueForChannel(queue, channel);
  }
};
export const registerActivity = async (message: IParsedMessage) => {
  await query(Queries.registerActivity, [
    // channel id
    _channelCache.get(message.params[0]) || "",
    // chatter id
    message.tags.get("user-id"),
    // message
    message.content,
  ]);
};
export const getUserData = async ({
  display,
  id,
}: IGetUserParameters): Promise<ITwitchUser | null> => {
  const foundPlayer = await query<ITwitchUser>(
    id ? Queries.findPlayerById : Queries.findPlayerByTwitchName,
    [id ? id : display]
  );
  if (foundPlayer.rowCount < 1) {
    return null;
  }
  const player = foundPlayer.rows[0];
  return player;
};
export const getReverseUserData = async (
  ign: string
): Promise<ITwitchUser[] | null> => {
  const foundPlayer = await query<ITwitchUser>(Queries.findPlayerByIgn, [
    ign.trim(),
  ]);
  if (foundPlayer.rowCount < 1) {
    return null;
  }
  return foundPlayer.rows;
};
export const upsertPlayer = async (userData: ITwitchUser) => {
  const upsertResult = await query<ITwitchUser>(Queries.savePlayer, [
    userData.ign,
    userData.login,
    userData.id,
    userData.display,
    userData.eve_id,
  ]);
  if (upsertResult.rowCount < 1) {
    throw Error(
      `Failed to create player record for '${userData.login}' (${userData.id})`
    );
  }
  return upsertResult.rows[0];
};
export const upsertCharacter = async (
  charId: number,
  userData: ESICharacter
) => {
  const upsertResult = await query<Partial<ESICharacter>>(
    Queries.saveCharacter,
    [
      charId,
      userData.name,
      userData.gender,
      userData.birthday,
      userData.race_id,
      userData.title || null,
      userData.description || null,
      userData.corporation_id || null,
      userData.alliance_id || null,
    ]
  );
  if (upsertResult.rowCount < 1) {
    throw Error(`Failed to save EVE Character in database`);
  }
  return upsertResult.rows[0];
};
export const getCommand = (msg: string): string[] => {
  return msg
    .replace(/[^a-zA-Z0-9\s\'\-\.\!\_]/gi, "")
    .trim()
    .split(" ")
    .map((x) => x.trim())
    .filter((x) => x.length > 0);
};
export const parseIGN = (msg: string): string => {
  return (
    msg
      .trim()
      // invalid characters
      .replace(/[^a-zA-Z0-9\s\'\-\.]/gi, "")
      // duplicate spaces
      .replace(/\s{2,}/g, " ")
  );
};

export const dispatchCommand = async (
  message: IParsedMessage
): Promise<string | undefined> => {
  if (!message.content || !message.content.startsWith("!")) {
    return;
  }
  const cmd = getCommand(message.content);
  if (
    cmd.length === 0 ||
    !["!ign", "!igs", "!igrs", "!ignpurge"].includes(cmd[0].toLocaleLowerCase())
  ) {
    return;
  }
  const userName =
    message.tags.get("display-name") || message.prefix.get("user") || "";
  const isAllowed =
    message.tags.get("mod") === "1" ||
    message.prefix.get("user") === message?.params[0]?.substring(1) ||
    message.tags.get("user-id") === "51175553"; // lumikkode

  if (cmd[0].toLocaleLowerCase() == "!igs") {
    // we search a given eve user based on twitch name
    await registerActivity(message);
    if (cmd.length === 1) {
      return;
    }
    if (!isAllowed) {
      return `@${userName}: Only broadcaster and mods can do that.`;
    }
    const twitchTargetUser = cmd.slice(1).join(" ").replace("@", "");
    const foundUser = await getUserData({
      display: twitchTargetUser,
    });
    if (foundUser) {
      return `@${userName}: Account "${twitchTargetUser}" registered IGN "${foundUser.ign}".`;
    } else {
      return `@${userName}: Account "${twitchTargetUser}" has not registered a IGN.`;
    }
  } else if (cmd[0].trim().toLocaleLowerCase() === "!igrs") {
    await registerActivity(message);
    if (cmd.length === 1) {
      return;
    }
    if (!isAllowed) {
      return;
    }
    const ingameTargetUser = parseIGN(cmd.slice(1).join(" "));
    const foundUser = await getReverseUserData(ingameTargetUser);
    if (!foundUser || foundUser.length == 0) {
      return format(
        '@%s: IGN "%s" not registered.',
        userName,
        ingameTargetUser
      );
    }

    if (foundUser.length > 1) {
      const userlist =
        foundUser.length > 5
          ? format(
              "(5 show, %s total) %s",
              foundUser.length,
              foundUser
                .slice(0, 4)
                .map((x) => x.login)
                .join(", ")
            )
          : foundUser.map((x) => x.login).join(", ");

      return format(
        "@%s: IGN %s is registered by the following users: %s",
        userName,
        foundUser[0].ign,
        userlist
      );
    }
    return format(
      '@%s: IGN "%s" belongs to "%s" (%s).',
      userName,
      foundUser[0].ign,
      foundUser[0].display,
      foundUser[0].login
    );
  } else if (cmd[0].trim().toLocaleLowerCase() === "!ign" && cmd.length === 1) {
    await registerActivity(message);
    // Request to show the user's ign
    const foundUser = await getUserData({
      id: message.tags.get("user-id"),
    });
    if (!foundUser) {
      return `@${userName}: No IGN registered.`;
    }
    return `@${userName}: IGN "${foundUser.ign}"`;
  } else if (cmd[0].toLocaleLowerCase() === "!ign" && cmd.length > 1) {
    await registerActivity(message);
    // Upsert the user informations
    const eveNick = parseIGN(cmd.slice(1).join(" "));
    let charId = null;
    try {
      // findCharacter
      const foundCharacter = await fetchCharacter(eveNick);
      if (foundCharacter) {
        charId = foundCharacter.id as number;
      }
    } catch (err) {
      // for some reason it failed
    }
    if (!charId) {
      try {
        charId = await searchCharacter(eveNick);
        if (!charId) {
          throw Error("No charId found");
        }
        const charData = await getCharacter(charId);
        if (!charData) {
          throw Error("No char data found");
        }
        await upsertCharacter(charId, charData);
      } catch (err) {
        // do nothing, optional
        Logger.info(
          err,
          "Here is an emitted error while searching for characters"
        );
      }
    }
    const userData = {
      display: message.tags.get("display-name"),
      id: message.tags.get("user-id") || "-1",
      ign: eveNick,
      eve_id: charId,
      login: message.prefix.get("user"),
    };
    try {
      await upsertPlayer(userData);
      return `@${userName}: IGN "${eveNick}" registered${
        !charId ? " but not found in Eve Online" : ""
      }.`;
    } catch (err) {
      return `@${userName}: Registration failed. Please try again.`;
    }
  } else if (cmd[0].toLocaleLowerCase() === "!ignpurge") {
    await registerActivity(message);
    // Request to show the user's ign
    const userid = message.tags.get("user-id");
    await query(Queries.unbind, [userid]);
    return format(
      "@%s: IGN purged from your twitch account, if any.",
      userName
    );
  }
};

export const searchCharacter = async (
  search: string
): Promise<number | null> => {
  const uri = `${baseURI}/universe/ids/`;
  const queryParameters = {
    datasource: "tranquility",
    language: "en",
  };
  const response = await applyBaseHeaders(post(uri))
    .query(queryParameters)
    .send([search]);
  Logger.info(response, "Result from the query");
  if (response.body) {
    const foundCharacters = response.body.characters;
    if (Array.isArray(foundCharacters) && foundCharacters.length > 0) {
      return foundCharacters[0].id as number;
    }
  }
  return null;
};
export const getCharacter = async (
  characterId: number
): Promise<ESICharacter | null> => {
  const uri = `${baseURI}/characters/${characterId}`;
  const queryParameters = {
    datasource: "tranquility",
  };
  const response = await applyBaseHeaders(get(uri))
    .query(queryParameters)
    .send();
  if (response.body && response.statusCode === 200) {
    return response.body as ESICharacter;
  }
  return null;
};
export const fetchCharacter = async (search: string) => {
  const results = await query<Partial<ESICharacter>>(Queries.findCharacter, [
    search,
  ]);
  if (results.rowCount < 1) {
    return null;
  }
  return results.rows[0];
};
