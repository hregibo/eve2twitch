import { createLogger } from "bunyan";
export const LoggingInstance = createLogger({
  environment: process.env.NODE_ENV,
  level: "debug",
  name: "Entity",
  streams: [],
});
if (process.env.ENVIRONMENT !== "test") {
  LoggingInstance.addStream({
    level: "debug",
    name: "STDOUT",
    stream: process.stdout,
    type: "stream",
  });
}
export default LoggingInstance;
