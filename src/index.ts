process.title = "Eve2Twitch";
import require_env from "@hregibo/require-env";
import { config } from "dotenv";
import { resolve } from "path";
config({
  path: resolve(__dirname, "../.env"),
});
require_env("dbc_string");

import { Client, IParsedMessage, IRC_TYPES } from "twsc";
import { LoggingInstance as Logger } from "./logger";
import * as Logic from "./logic";
import * as Queries from "./queries";

import { IQueueMessage, MessageQueue } from "@hregibo/message-queue";
import { query } from "@hregibo/pg-dbc";

(async () => {
  Logger.info("Starting Eve2Twitch...");
  // Load the program
  try {
    // migrate
    try {
      await query(Queries.createChannelsTable, []);
      await query(Queries.createPlayersTable, []);
      await query(Queries.createActivityTable, []);
      await query(Queries.createEveCharacterTable, []);
      await query(Queries.alterPlayerTableAddEveId, []);
    } catch (err) {
      Logger.fatal(
        err,
        "Failed to connect to database and run migration scripts."
      );
      process.exit(1);
    }
    Logger.info("Migration over, connecting");

    const q = new MessageQueue();
    q.pollingDelay = 750;
    Logic.createBaseQueue(q);
    const c = new Client({
      capabilities: [
        "twitch.tv/membership",
        "twitch.tv/commands",
        "twitch.tv/tags",
      ],
      nickname: process.env.CLIENT_NICK || "",
      password: process.env.CLIENT_PASS,
    });
    const dequeueListener = (msq: IQueueMessage) => {
      Logger.debug(
        {
          ...msq,
          _now: new Date().toString(),
        },
        "dequeueing message"
      );
      c.emit("send", msq.message);
    };
    q.on("dequeue", dequeueListener);
    // events
    c.on("ready", async () => {
      Logger.info("Eve2Twitch is now ready for messages");
      await Logic.triggerJoinRequests(q);
      Logger.debug(q, "about to start the queue loop");
      q.emit("start");
    });
    c.on("closed", (code, reason) => {
      Logger.warn("Connection has closed.", { code, reason });
      q.emit("stop");
      q.emit("purge", "base");
    });
    c.on("reconnect", () => Logger.warn("reconnecting"));
    c.on("message", async (msg: IParsedMessage) => {
      if (msg.type === IRC_TYPES.PRIVMSG) {
        const channel = msg.params[0];
        const reply = await Logic.dispatchCommand(msg);
        if (reply) {
          q.emit("enqueue", {
            message: `PRIVMSG ${channel} :${reply}`,
            queue: channel,
          });
          Logger.info(
            {
              request: msg.raw,
              response: reply,
            },
            "Enqueued this message"
          );
        }
      }
    });

    const exitFn = () => {
      c.emit("stop");
      q.emit("stop");
      process.exit(0);
    };
    process.on("SIGTERM", exitFn);
    process.on("SIGINT", exitFn);
    c.emit("start");
  } catch (e) {
    Logger.fatal(e, "Fatal error while running Eve2Twitch");
    process.exit(1);
  }
})();
