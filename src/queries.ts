export const createChannelsTable = `CREATE TABLE IF NOT EXISTS channels (
    id VARCHAR(60) NOT NULL PRIMARY KEY,
    name VARCHAR(60) NOT NULL,
    active BOOLEAN DEFAULT true
);`;
export const createPlayersTable = `CREATE TABLE IF NOT EXISTS players (
    id VARCHAR(60) NOT NULL PRIMARY KEY,
    login VARCHAR(60) NOT NULL,
    display VARCHAR(60) DEFAULT NULL,
    ign VARCHAR(300) DEFAULT NULL,
    created_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP WITH TIME ZONE DEFAULT NULL
);`;
export const createEveCharacterTable = `CREATE TABLE IF NOT EXISTS eve_characters (
    id INTEGER NOT NULL PRIMARY KEY,
    name VARCHAR(128) NOT NULL,
    gender VARCHAR(60) NOT NULL,
    birthdate TIMESTAMP WITH TIME ZONE NOT NULL,
    race_id INTEGER DEFAULT NULL,
    title VARCHAR(255) DEFAULT NULL,
    description VARCHAR DEFAULT NULL,
    corp_id INTEGER DEFAULT NULL,
    alliance_id INTEGER DEFAULT NULL
);`;
export const alterPlayerTableAddEveId = `ALTER TABLE players ADD COLUMN IF NOT EXISTS eve_id INTEGER DEFAULT NULL;`;

export const createTokenTable = `CREATE TABLE IF NOT EXISTS esi_tokens (
  character_id BIGINT NOT NULL PRIMARY KEY,
  access_token VARCHAR(128) NOT NULL,
  refresh_token VARCHAR(128) NOT NULL,
  expires_in BIGINT NOT NULL,
  created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP,
);`;

export const getChannels = `SELECT name, id FROM channels;`;
export const findPlayerById = `SELECT ign, login, display FROM players WHERE id = $1 LIMIT 1;`;

export const findPlayerByTwitchName = `SELECT ign, login, display FROM players WHERE LOWER(login) = LOWER($1) OR LOWER(display) = LOWER($1) LIMIT 1;`;
export const findPlayerByIgn = `SELECT ign, login, display FROM players WHERE LOWER(ign) = LOWER($1);`;

export const savePlayer = `INSERT INTO players (ign, login, id, display, eve_id)
VALUES ($1, $2, $3, $4, $5)
ON CONFLICT (id) DO UPDATE
SET     login = EXCLUDED.login,
        display = EXCLUDED.display,
        ign = EXCLUDED.ign,
        eve_id = EXCLUDED.eve_id, 
        updated_at = CURRENT_TIMESTAMP
RETURNING ign, login, created_at, updated_at, display;`;

export const createActivityTable = `CREATE TABLE IF NOT EXISTS activity (
    id BIGSERIAL NOT NULL PRIMARY KEY,
    channel_id VARCHAR(60) NOT NULL,
    chatter_id VARCHAR(60) NOT NULL,
    date TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    command VARCHAR(512) DEFAULT NULL
);`;
export const registerActivity = `INSERT INTO activity (channel_id, chatter_id, command) VALUES ($1, $2, $3);`;
export const saveCharacter = `INSERT INTO eve_characters (
    id, name, gender, birthdate, race_id, title, description, corp_id, alliance_id
) VALUES (
    $1, $2, $3, $4, $5, $6, $7, $8, $9
) RETURNING id, name;`;
export const findCharacter = `SELECT id, name FROM eve_characters WHERE name = $1 LIMIT 1;`;

export const getToken = `SELECT character_id, access_token, refresh_token, expires_in, created_at FROM esi_tokens LIMIT 1;`;

export const setToken = `INSERT INTO esi_tokens (character_id, access_token, refresh_token, expires_in) VALUES ($1, $2, $3, $4) ON CONFLICT (character_id) DO UPDATE SET access_token = EXCLUDED.access_token, refresh_token = EXCLUDED.refresh_token, expires_in = EXCLUDED.expires_in;`;

export const unbind = "DELETE FROM players WHERE id = $1";
