FROM node:18-slim

WORKDIR /home/node
RUN apt update --quiet && apt install --assume-yes --no-install-recommends python3 make gcc

COPY ./package.json .
COPY ./package-lock.json .

RUN npm ci
RUN npm cache clear --force

COPY . .
RUN npm run build
RUN chown node:node -R /home/node

USER node
CMD ["node", "./lib/index.js"]

